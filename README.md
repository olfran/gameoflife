# Game Of Life #
This is My attempt on creating a version of the [Conway's Game of Life](https://en.wikipedia.org/wiki/Conway%27s_Game_of_Life)

It uses DIVs tags and pure DOM with JQuery for the cells.
The cells are rendered when its state changes in order to avoid rendering the whole field again.

Tech: JQuery, HTML5, CSS3
Tested on: Google Chrome and Firefox

By Olfran
Twitter: [@nullxor](https://twitter.com/nullxor)
Linkedin: [https://linkedin.com/in/olfran](https://linkedin.com/in/olfran)