(function (window, $) {
/*
* Ejemplo simple y algo optimizado del juego de la vida de Conway
* Se crea un objeto constructor del tipo GameOfLife que recibe como
* parámetros el contenedor donde dibujar el campo, el ancho y el alto
* del campo.
* 
* Nota: Sólo se dibujan las células si han cambiado de estado
* es decir no se dibuja nuevamente el campo entero.
* 
* http://es.wikipedia.org/wiki/Juego_de_la_vida
*
* Tecnología usada: JQuery, HTML5, CSS3
* Probado en: Google Chrome y Firefox
*
* Por Olfran Jiménez <olfran@gmail.com>
* Twitter: @nullxor
* Linkedin: ve.linkedin.com/in/olfran
*/

function GameOfLife(container, width, height) {
	this._width = height;
	this._height = width;
	//Matriz del campo 1 = célula viva; 0 = célula muerta
	this._matrix = []; 

	//Contenedor donde colocar las células
	this._container = container;
	this._paused = false;
	this._iteration = 0;
}

//Dibujar el campo y construir la matriz
GameOfLife.prototype.makeField = function () {
	for (var w = 0 ; w < this._width; w++) {
		this._matrix[w] = [];
		for (var h = 0; h < this._height; h++) {
			var element = document.createElement("div");
			element.className = "dead";
			element.id = "f_" + w + "_" + h;
			this._container.appendChild(element);
			this._matrix[w].push(0);
		}
	}
}

//Cambiar el estado de una célula 1 = Viva; 0 = Muerta
GameOfLife.prototype.setCell = function (x, y, status) {
	try {
		var className = status ? "alive" : "dead";
		var id = "#f_" + x + "_" + y;
		$(id).attr("class", className);
		this._matrix[x][y] = Number(status);
	}
	catch(ex) {
		return false;
	}
	return true;
}

GameOfLife.prototype.getCell = function (x, y) {
	try {
		if (x >= this._width) { x = 0; }
		if (x < 0) { x = this._width - 1; }
		if (y >= this._height) { y = 0; }
		if (y < 0) { y = this._height - 1; }

		return this._matrix[x][y] || 0;
	}
	catch(ex) {
		return 0;
	}
}

//Hacer las iteraciones
GameOfLife.prototype.iterate = function (force) {
	if (this._paused && !force) {
		return false;
	}

	var coordenadas = [];

	for (var w = 0 ; w < this._width; w++) {
		for (var h = 0; h < this._height; h++) {
			var status = this.getCell(w, h);
			var vecinos = this.getNeighbours(w, h);

			//Muere por falta de población o por superpoblación
			if ((vecinos < 2 || vecinos > 3) && status == 1) {
				coordenadas.push({x: w, y: h, status: 0});
			}
			//Si tiene 3 vecinos y está muerta, nace una nueva célula
			if (vecinos == 3 && status == 0) {
				coordenadas.push({x: w, y: h, status: 1});
			}
		}
	}
	if (coordenadas) {
		for (var i = 0; i < coordenadas.length; i++) {
			this.setCell(coordenadas[i].x, coordenadas[i].y, coordenadas[i].status);
		}
	}
	this._iteration++;
}

//Regresa el número de vecinos de la célula
GameOfLife.prototype.getNeighbours = function (x, y) {
	var result = [];
	x = Number(x); y = Number(y);
	
	//ToDo: Buscar una forma menos bestia de hacer esto! :D
	//Arriba
	result.push(this.getCell(x, y - 1));
	result.push(this.getCell(x - 1, y - 1));
	result.push(this.getCell(x + 1, y - 1));
	
	//Izquierda Derecha
	result.push(this.getCell(x - 1, y));
	result.push(this.getCell(x + 1, y));

	//Abajo
	result.push(this.getCell(x, y + 1));
	result.push(this.getCell(x - 1, y + 1));
	result.push(this.getCell(x + 1, y + 1));

	return result.reduce(function (a, b) { return a + b; });
}

GameOfLife.prototype.getPopulation = function () {
	var population = 0;
	for (var w = 0 ; w < this._width; w++) {
		for (var h = 0; h < this._height; h++) {
			population += this.getCell(w, h);
		}
	}
	return population;
}

GameOfLife.prototype.getIteration = function () {
	return this._iteration;
}

GameOfLife.prototype.next = function () {
	if (this._paused) {
		this.iterate(true);
	}
}

GameOfLife.prototype.pause = function (state) {
	this._paused = state;
}

GameOfLife.prototype.restart = function () {
	this._iteration = 0;
	for(var x = 0; x < this._width; x++) {
		for(var y = 0; y < this._height; y++) {
			this.setCell(x, y, 0);
		}
	}
}

GameOfLife.prototype.random = function () {
	this._iteration = 0;
	for(var x = 0; x < this._width; x++) {
		for(var y = 0; y < this._height; y++) {
			var status = Math.round(Math.random());
			this.setCell(x, y, status);
		}
	}
}

window.GameOfLife = GameOfLife;
})(window, $);