$(function () {
	//Tamaño inicial del campo
	var width = 80;
	var height = 40; 
	//var width = $(window).width(), height = $(window).height();

	var gol = new GameOfLife(document.getElementById("field"), width, height);
	gol.makeField();
	setInterval(iterate, 50);

	$("div#field > div").click(function () {
		var status = $(this).attr("class") == "alive" ? 0 : 1;
		var splitted = $(this).attr("id").split("_");
		var x = splitted[1];
		var y = splitted[2];
		gol.setCell(x, y, status);
	});

	$("#pause").click(function () {
		gol.pause(true);
	});

	$("#play").click(function () {
		gol.pause(false);
	});

	$("#next").click(function () {
		gol.next();
		$("#population").html("<strong>Población: </strong>" + gol.getPopulation() + "<strong>Iteración: </strong>" + gol.getIteration());
	});

	$("#random").click(function () {
		gol.random();
	});

	$("#restart").click( function () {
		gol.restart();
	});

	function iterate() {
		gol.iterate();
		$("#population").html("<strong>Población: </strong>" + gol.getPopulation() + "<strong> Iteración: </strong>" + gol.getIteration());
	}
	gol.random();
});